package com.zimanpro;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class LockScreenBroadcastReceiver extends BroadcastReceiver {
    Context context;
    @Override
    public void onReceive(final Context context, final Intent intent) {
//        Log.e("LOB","onReceive");
        this.context = context;


//        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
//            // do whatever you need to do here
//            wasScreenOn = false;
////            Log.e("LOB","wasScreen = "+wasScreenOn);
//
//        sessionManager = new SessionManager(context);
//        GPSTracker gpsTracker = new GPSTracker(context);
//        // check if GPS enabled
//        if (gpsTracker.canGetLocation()) {
//            Location location = gpsTracker.getLocation();
//            if (location != null) {
//                latitude = location.getLatitude();
//                longitude = location.getLongitude();
//
//                System.out.println("latitude = " + latitude);
//                System.out.println("longitude = " + longitude);
//            }
//        }
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                Count = 0;
//            }
//        }, 1000);


        PowerButtonService mYourService = new PowerButtonService();
        Intent mServiceIntent = new Intent(context, mYourService.getClass());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startForegroundService(mServiceIntent);
                System.out.println("Foreground Service in LockScreenBroadcastReceiver");
            }

            //System.out.println("LockScreenBroadcastReceiver above oreo");
        } else {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startService(mServiceIntent);
                System.out.println("Start Service in LockScreenBroadcastReceiver");
            }

            //System.out.println("LockScreenBroadcastReceiver below oreo");
        }

        /*LockService mYourService = new LockService();
        Intent mServiceIntent = new Intent(context, mYourService.getClass());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startForegroundService(mServiceIntent);
            }
        } else {
            if (!isMyServiceRunning(mYourService.getClass())) {
                context.startService(mServiceIntent);
            }
        }*/

//        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            // and do whatever you need to do here
//            wasScreenOn = true;
//            Count++;
//            if(Count==2){
//                //Send SMS code..
//                Log.e("LOB","Count is two 2");
//            }
//
//        } else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
////            Log.e("LOB","userpresent wasScreen = "+wasScreenOn);
////            String url = "http://www.stackoverflow.com";
////            Intent i = new Intent(Intent.ACTION_VIEW);
////            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////            i.setData(Uri.parse(url));
////            context.startActivity(i);
//        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
//                Log.i("Service status1", "Running");
                return true;
            }
        }
//        Log.i("Service status1", "Not running");
        return false;
    }


//    private void createPanicRequest() {
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppDataUrls.postCreatePanicRequest(),
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("createPanicRequest = ", response);
//
////                        try {
////                            JSONObject jsonObject = new JSONObject(response);
////                            if (jsonObject.getString("status").equals(AppConstants.SUCCESS)) {
//////                                int result = jsonObject.getInt("result");
//////                                panicId = result;
////                            }
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put(AppConstants.USER_ID, sessionManager.getUserId());
//                params.put(AppConstants.AUTH_KEY, sessionManager.getAuthKey());
//                params.put(AppConstants.APP_SECURITY_KEY, AppConstants.APP_SECURITY_KEY_VALUE);
//                params.put(AppConstants.USER_LAT, String.valueOf(latitude));
//                params.put(AppConstants.USER_LONG, String.valueOf(longitude));
//
//                System.out.println("Create Panic Params = " + params.toString());
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_WWW);
//                return params;
//            }
//        };
//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        ZimanProApplication.getInstance().addToRequestQueue(stringRequest);
//    }

}
