package com.zimanpro.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zimanpro.R;
import com.zimanpro.database.DatabaseHelper;
import com.zimanpro.pojo.get_notification.Bulletin;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BulletinListActivity extends AppCompatActivity {

    DatabaseHelper db;
    List<Bulletin> bulletinList;
    TextView textViewNoNotifications;
    RecyclerView recyclerViewNotification;
    CustomAdapter customAdapter;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulletin_list);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            setActionBarTitleText();
        }

        bulletinList = new ArrayList<>();
        db = new DatabaseHelper(this);

        bulletinList = db.getAllBulletineData();

        bulletinList.sort(new IdSorter());

        textViewNoNotifications = findViewById(R.id.textViewNoNotifications);
        recyclerViewNotification = findViewById(R.id.recyclerViewNotification);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewNotification.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
        customAdapter = new CustomAdapter();
        recyclerViewNotification.setAdapter(customAdapter); // set the Adapter to RecyclerView

        System.out.println("Size: " + bulletinList.size());
    }

    private void setActionBarTitleText() {
        ActionBar ab = getSupportActionBar();
        LayoutInflater inflater = getLayoutInflater();
        View customLayout = inflater.inflate(R.layout.abs_layout_notifications, null);

        ImageView imageViewBackArrow = customLayout.findViewById(R.id.imageViewBackArrow);
        TextView textView = customLayout.findViewById(R.id.tvTitle);

        textView.setText("Bulletin News");
        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(customLayout);
    }

    public class IdSorter implements Comparator<Bulletin> {
        @Override
        public int compare(Bulletin o1, Bulletin o2) {
            return o2.createdAt.compareTo(o1.createdAt);
        }
    }

    public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification_layout, parent, false);
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

            if (!bulletinList.isEmpty()) {
                holder.textViewNotificationTitle.setText(bulletinList.get(position).title);
                holder.textViewNotificationDesc.setText(bulletinList.get(position).message);
                holder.textViewNotificationDate.setText(bulletinList.get(position).createdAt);

                holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(BulletinListActivity.this, ShowNotificationActivity.class);
                        intent.putExtra("from","Bulletin");
                        intent.putExtra("notificationBodyString", bulletinList.get(position).message);
                        intent.putExtra("notificationTitleString", bulletinList.get(position).title);
                        intent.putExtra("image", bulletinList.get(position).image);
                        intent.putExtra("notificationDate", bulletinList.get(position).createdAt);
                        startActivity(intent);
                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return bulletinList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textViewNotificationTitle;// init the item view's
            TextView textViewNotificationDesc;// init the item view's
            TextView textViewNotificationDate;// init the item view's
            LinearLayout linearLayout;

            MyViewHolder(View itemView) {
                super(itemView);

                textViewNotificationTitle = (TextView) itemView.findViewById(R.id.textViewNotificationTitle);
                textViewNotificationDesc = (TextView) itemView.findViewById(R.id.textViewNotificationDesc);
                textViewNotificationDate = (TextView) itemView.findViewById(R.id.textViewNotificationDate);
                linearLayout = itemView.findViewById(R.id.lay1);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}