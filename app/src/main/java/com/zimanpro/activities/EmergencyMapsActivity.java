package com.zimanpro.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapmyindia.sdk.direction.ui.DirectionCallback;
import com.mapmyindia.sdk.direction.ui.DirectionFragment;
import com.mapmyindia.sdk.direction.ui.model.DirectionOptions;
import com.mapmyindia.sdk.direction.ui.model.DirectionPoint;
import com.mapmyindia.sdk.nearby.plugin.CategoryCode;
import com.mapmyindia.sdk.nearby.plugin.MapmyIndiaNearbyWidget;
import com.mapmyindia.sdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mmi.services.api.directions.models.DirectionsResponse;
import com.mmi.services.api.nearby.model.NearbyAtlasResult;
import com.zimanpro.R;
import com.zimanpro.mapSetting.MapmyIndiaDirectionWidgetSetting;

import java.util.ArrayList;
import java.util.List;

public class EmergencyMapsActivity extends AppCompatActivity /*implements com.mapbox.mapboxsdk.maps.OnMapReadyCallback*/ {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    protected static final String INFO_TAG = "PANIC_MV_A";
    public ArrayList<String> markerIds = new ArrayList<String>();
    public ArrayList<String> placeIds = new ArrayList<String>();
    private GoogleMap googleMap;
    private Activity ctx;
    private CardView cardViewDetails;
    private TextView textViewLocationName;
    private TextView textViewAddress;
    private LinearLayout ll_call;
    private LinearLayout ll_get_direction;
    private String jsonResponse = "", apiKey = "", jsonResponse1 = "", jsonResponse2 = "", jsonResponse3 = "";
    private Location location;
    MarkerOptions mypos;

    private MapboxMap mapmyIndiaMap;
    private MapView mapView;
    int radius = 1000;
    com.mapbox.mapboxsdk.annotations.MarkerOptions markerOptions;
    FrameLayout frameLayout;
    ArrayList<CategoryCode> codeArrayList = new ArrayList<>();
    ArrayList<String> policeList, fireList, atmList, hospitalList;
    String Category[] = {"POLSTN", "POLOTH", "POLCWK", "POLOFC", "COMFIR", "FINATM"};


    //private ActivityNearbyPluginBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosmap);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        ctx = this;

        frameLayout = findViewById(R.id.fragment_container);

        /*Intent intent = new MapmyIndiaNearbyWidget.IntentBuilder().build(EmergencyMapsActivity.this);
        startActivityForResult(intent, 101);*/

        policeList = new ArrayList<>();
        policeList.add("POLSTN");
        policeList.add("POLOTH");
        policeList.add("POLCWK");
        policeList.add("POLOFC");

        fireList = new ArrayList<>();
        fireList.add("COMFIR");

        atmList = new ArrayList<>();
        atmList.add("FINATM");

        hospitalList = new ArrayList<>();
        hospitalList.add("HLTHSP");
        hospitalList.add("HSPVTH");
        hospitalList.add("HSPEYH");
        hospitalList.add("HSPDNH");
        hospitalList.add("HSPCHD");
        hospitalList.add("HSPMAT");
        hospitalList.add("HSPAUR");
        hospitalList.add("HSPORH");
        hospitalList.add("HSPHMH");
        hospitalList.add("HSPENT");
        hospitalList.add("HSPHRH");
        hospitalList.add("HSPCAN");
        hospitalList.add("HSPDPT");
        hospitalList.add("HSPMNH");
        hospitalList.add("HSPNAT");
        hospitalList.add("HSPURO");

        Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.burger_button);

        int height = 70;
        int width = 70;
        Bitmap b1 = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_police_new);
        Bitmap policeMarker = Bitmap.createScaledBitmap(b1, width, height, false);

        Bitmap b2 = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_hospital_new);
        Bitmap hospitalMarker = Bitmap.createScaledBitmap(b2, width, height, false);

        Bitmap b3 = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.ic_fire_station_new);
        Bitmap fireMarker = Bitmap.createScaledBitmap(b3, width, height, false);

        CategoryCode categoryCode = null;
        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                categoryCode = new CategoryCode("Police Station", b, policeList, policeMarker);
            }
            if (i == 1) {
                categoryCode = new CategoryCode("Fire Station", b, fireList, fireMarker);
            }
            if (i == 2) {
                categoryCode = new CategoryCode("ATM", b, atmList, fireMarker);
            }
            if (i == 3) {
                categoryCode = new CategoryCode("Hospitals", b, hospitalList, hospitalMarker);
            }
            codeArrayList.add(categoryCode);
        }

        Intent intent = new MapmyIndiaNearbyWidget.IntentBuilder().setCategoryList(codeArrayList).build(EmergencyMapsActivity.this);
        //startActivity(intent);
        startActivityForResult(intent, 101);
        finish();

        /*Intent intent = new MapmyIndiaNearbyWidget.IntentBuilder().build(this);
        startActivityForResult(intent, 101);*/

        /*mapView = findViewById(R.id.map_view);
        mapView.getMapAsync(this);

        cardViewDetails = (CardView) findViewById(R.id.cardViewDetails);
        textViewLocationName = (TextView) findViewById(R.id.textViewLocationName);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        ll_call = (LinearLayout) findViewById(R.id.ll_call);
        ll_get_direction = (LinearLayout) findViewById(R.id.ll_get_direction);*/

        /*MapmyIndiaNearbyFragment nearbyFragment = MapmyIndiaNearbyFragment.newInstance();

        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.fragment_container, nearbyFragment, MapmyIndiaNearbyFragment.class.getSimpleName())
                .commit();

        nearbyFragment.setMapmyIndiaNearbyCallback(nearbyAtlasResult -> {
            // getSupportFragmentManager().beginTransaction().remove(nearbyFragment).commit();

            Toast.makeText(EmergencyMapsActivity.this, nearbyAtlasResult.placeAddress, Toast.LENGTH_SHORT).show();
        });*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            NearbyAtlasResult result = MapmyIndiaNearbyWidget.getNearbyResponse(data);
            String address = result.getPlaceAddress();
            String placeName = result.getPlaceName();
            String eLoc = result.geteLoc();

            System.out.println("address: " + address + " placeName: " + placeName + " eLoc: " + eLoc);
            //showDirection(address, placeName, eLoc);
            //startActivity(new Intent(EmergencyMapsActivity.this, DirectionW.class));
            //Toast.makeText(EmergencyMapsActivity.this, result.placeAddress, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }

    private void showDirection(String address1, String placeName1, String eLoc1){
        DirectionOptions.Builder optionsBuilder = DirectionOptions.builder();
        if (!MapmyIndiaDirectionWidgetSetting.getInstance().isDefault()){

            //MapmyIndiaDirectionWidgetSetting.getInstance().setDestination(destination);
            PlaceOptions options = PlaceOptions.builder()
                    .zoom(MapmyIndiaDirectionWidgetSetting.getInstance().getZoom())
                    .hint(MapmyIndiaDirectionWidgetSetting.getInstance().getHint())
                    .location(MapmyIndiaDirectionWidgetSetting.getInstance().getLocation())
                    .filter(MapmyIndiaDirectionWidgetSetting.getInstance().getFilter())
                    .saveHistory(MapmyIndiaDirectionWidgetSetting.getInstance().isEnableHistory())
                    .pod(MapmyIndiaDirectionWidgetSetting.getInstance().getPod())
                    .attributionHorizontalAlignment(MapmyIndiaDirectionWidgetSetting.getInstance().getSignatureVertical())
                    .attributionVerticalAlignment(MapmyIndiaDirectionWidgetSetting.getInstance().getSignatureHorizontal())
                    .logoSize(MapmyIndiaDirectionWidgetSetting.getInstance().getLogoSize())
                    .tokenizeAddress(MapmyIndiaDirectionWidgetSetting.getInstance().isTokenizeAddress())
                    .historyCount(MapmyIndiaDirectionWidgetSetting.getInstance().getHistoryCount())
                    .backgroundColor(getResources().getColor(MapmyIndiaDirectionWidgetSetting.getInstance().getBackgroundColor()))
                    .toolbarColor(getResources().getColor(MapmyIndiaDirectionWidgetSetting.getInstance().getToolbarColor()))
                    .build();


            optionsBuilder.searchPlaceOption(options)
                    .showStartNavigation(MapmyIndiaDirectionWidgetSetting.getInstance().isShowStartNavigation())
                    .steps(MapmyIndiaDirectionWidgetSetting.getInstance().isSteps())
                    .resource(MapmyIndiaDirectionWidgetSetting.getInstance().getResource())
                    .profile(MapmyIndiaDirectionWidgetSetting.getInstance().getProfile())
                    .excludes(MapmyIndiaDirectionWidgetSetting.getInstance().getExcludes())
                    .overview(MapmyIndiaDirectionWidgetSetting.getInstance().getOverview())
                    .showAlternative(MapmyIndiaDirectionWidgetSetting.getInstance().isShowAlternative());
        }

        DirectionFragment directionFragment = DirectionFragment.newInstance(optionsBuilder.destination(DirectionPoint.setDirection(placeName1, address1, eLoc1)).build());

        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.fragment_container, directionFragment, DirectionFragment.class.getSimpleName()).
                commit();

        directionFragment.setDirectionCallback(new DirectionCallback() {
            @Override
            public void onCancel() {
                finish();
            }

            @Override
            public void onStartNavigation(DirectionPoint directionPoint, DirectionPoint directionPoint1, List<DirectionPoint> list, DirectionsResponse directionsResponse, int i) {
                Toast.makeText(EmergencyMapsActivity.this, "On Navigation Start", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
