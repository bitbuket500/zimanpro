package com.zimanpro.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.zimanpro.R;

public class ShowNotificationActivity extends AppCompatActivity {

    String notificationBodyString = "", notificationTitleString = "", notificationDate = "", from, image;
    TextView textViewMsg, textViewDate;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_notification);

        Intent intent = getIntent();
        if (intent != null) {
            from = intent.getStringExtra("from");
            notificationTitleString = intent.getStringExtra("notificationTitleString");
            notificationBodyString = intent.getStringExtra("notificationBodyString");
            notificationDate = intent.getStringExtra("notificationDate");
            System.out.println("notificationBodyString = " + notificationBodyString);

            if (from.equals("Bulletin")){
                image = intent.getStringExtra("image");
            }
        }

        textViewMsg = findViewById(R.id.textmsg);
        textViewDate = findViewById(R.id.textdate);
        imageView = findViewById(R.id.imageview);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            setActionBarTitleText(notificationTitleString);
            //setActionBarTitleText("Notifications");
        }

        textViewMsg.setText(notificationBodyString);
        textViewDate.setText(notificationDate);

        if (from.equals("Bulletin")) {
            imageView.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round);

            Glide.with(this)
                    .asBitmap()
                    .load(image)
                    //.apply(options)
                    .into(imageView);
        }else {
            imageView.setVisibility(View.GONE);
        }

    }

    private void setActionBarTitleText(String notificationTitleString) {
        ActionBar ab = getSupportActionBar();
        LayoutInflater inflater = getLayoutInflater();
        View customLayout = inflater.inflate(R.layout.abs_layout_notifications, null);

        TextView textViewTitle = customLayout.findViewById(R.id.tvTitle);
        ImageView imageViewBackArrow = customLayout.findViewById(R.id.imageViewBackArrow);
        textViewTitle.setText(notificationTitleString);
        imageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(customLayout);
    }


}