package com.zimanpro.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mapbox.mapboxsdk.MapmyIndia;
import com.mmi.services.account.MapmyIndiaAccountManager;
import com.zimanpro.R;
import com.zimanpro.helperClasses.SessionManager;

import ai.haptik.android.sdk.InitData;
//import io.fabric.sdk.android.Fabric;
import java.io.File;

import ai.haptik.android.sdk.HaptikLib;
import ai.haptik.android.sdk.picassohelper.PicassoApiFactory;

public class ZimanProApplication extends Application implements LifecycleObserver {

    public static final String TAG = ZimanProApplication.class.getSimpleName();
    private static Context context;
    private static ZimanProApplication mInstance;
    private RequestQueue mRequestQueue;


    public static Context getAppContext() {
        return ZimanProApplication.context;
    }

    public static synchronized ZimanProApplication getInstance() {
        return mInstance;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        //MultiDex.install(this);
    }

    SessionManager sessionManager;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        //Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        crashlytics.log("");

        ZimanProApplication.context = getApplicationContext();
        FirebaseApp.initializeApp(this);

        mInstance = this;
        sessionManager = new SessionManager(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(mInstance);

        MapmyIndiaAccountManager.getInstance().setRestAPIKey(getRestAPIKey());
        MapmyIndiaAccountManager.getInstance().setMapSDKKey(getMapSDKKey());
        MapmyIndiaAccountManager.getInstance().setAtlasClientId(getAtlasClientId());
        MapmyIndiaAccountManager.getInstance().setAtlasClientSecret(getAtlasClientSecret());
        MapmyIndia.getInstance(mInstance);
        //MapmyIndia.getInstance(this);

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("token App = " + token);
        if (!TextUtils.isEmpty(token)) {
             sessionManager.saveFCMToken(token);
        }

        HaptikLib.setRunEnvironment(HaptikLib.RUN_ENVIRONMENT_PRODUCTION);
        InitData initData = new InitData.Builder(this)
                .baseUrl("https://ziman.haptikapi.com/")
                .notificationSound(R.raw.beep)
                .imageLoadingService(PicassoApiFactory.getPicassoApi())
                .build();
        HaptikLib.init(initData);


        // register to be informed of activities starting up
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity arg0, Bundle arg1) {
                // new activity created; force its orientation to portrait
                arg0.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            @Override
            public void onActivityDestroyed(Activity arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onActivityPaused(Activity arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onActivityResumed(Activity arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onActivitySaveInstanceState(Activity arg0, Bundle arg1) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onActivityStarted(Activity arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onActivityStopped(Activity arg0) {
                // TODO Auto-generated method stub

            }
        });
    }

    // Adding some callbacks for test and log
    public interface ValueChangeListener {
        void onChanged(Boolean value);
    }

    public String getAtlasClientId() {
        return "33OkryzDZsK0XFjKL2rBF6aBBioj_PIrt6_L5-VYRokH4VatYsVmFsdoRyUY_uPTN3CN2hbjD510ZSFSyQVAjw==";
    }

    public String getAtlasClientSecret() {
        return "lrFxI-iSEg9kEIJHiEjFE-3VAkFMvs2jduAN8VUfrlQySt4DzrQscCgjwSEEAcS-sykfsdQEPG3DQant78UYwWg3hQsGxQ9L";
    }

    public String getMapSDKKey() {
        return "0ac6a71fbf6eaa03f0e65283367e555c";
    }

    public String getRestAPIKey() {
        return "0ac6a71fbf6eaa03f0e65283367e555c";
    }

    private ValueChangeListener visibilityChangeListener;
    public void setOnVisibilityChangeListener(ValueChangeListener listener) {
        this.visibilityChangeListener = listener;
    }
    private void isAppInBackground(Boolean isBackground) {
        if (null != visibilityChangeListener) {
            visibilityChangeListener.onChanged(isBackground);
        }
    }

    /*private static AppController mInstance;
    public static AppController getInstance() {
        return mInstance;
    }*/

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.d("MyApp", "App in background");
        isAppInBackground(true);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.d("MyApp", "App in foreground");
        isAppInBackground(false);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            HurlStack stack = new HurlStack() {
//                @Override
//                public HttpResponse performRequest(Request<?> request, Map<String, String> headers)
//                        throws IOException, AuthFailureError {
//                    return super.performRequest(request, headers);
//                }
            };
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), stack);
        }

//        if (mRequestQueue == null) {
//            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s + " DELETED");
                }
            }
        }
    }
}