package com.zimanpro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.zimanpro.pojo.get_notification.Bulletin;
import com.zimanpro.pojo.get_notification.Result;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    //Database Name
    public static final String DATABASE_NAME = "ZIMAN_NOTI_DB";

    //Database version
    public static final int DATABASE_VERSION = 6;
    Context mContext;

    //Notification table name
    private static final String TABLE_NOTIFICATION = "notification_records";

    //Bulletin table name
    private static final String TABLE_BULLETINE = "bulletine_records";

    //Visitor prim key name
    private static final String KEY_NOTIFICATION_PRIM_KEY = "notification_prim_key";

    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_IMAGE = "image";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //sqLiteDatabase.execSQL(NotificationDB.CREATE_TABLE);

        String CREATE_TABLE_VISITOR_MANAGER = "CREATE TABLE " + TABLE_NOTIFICATION + "(" + KEY_NOTIFICATION_PRIM_KEY + " INTEGER PRIMARY KEY,"
                + COLUMN_TITLE + " TEXT, "
                + COLUMN_MESSAGE + " TEXT, "
                + COLUMN_DATE + " TEXT " + ")";
        sqLiteDatabase.execSQL(CREATE_TABLE_VISITOR_MANAGER);

        String CREATE_TABLE_BULLETINE = "CREATE TABLE " + TABLE_BULLETINE + "(" + KEY_NOTIFICATION_PRIM_KEY + " INTEGER PRIMARY KEY,"
                + COLUMN_TITLE + " TEXT, "
                + COLUMN_MESSAGE + " TEXT, "
                + COLUMN_IMAGE + " TEXT, "
                + COLUMN_DATE + " TEXT " + ")";
        sqLiteDatabase.execSQL(CREATE_TABLE_BULLETINE);

        Log.d("Tag", "DataBase Executed");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        onCreate(db);
    }

    public long getNotificationCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_NOTIFICATION);
        db.close();
        return count;
    }

    public void clearDatabase(String TABLE_NAME) {
        SQLiteDatabase db = this.getReadableDatabase();
        String clearDBQuery = "DELETE FROM " + TABLE_NAME;
        db.execSQL(clearDBQuery);
    }

    public void addBulletineNotificationTable(Bulletin model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        //cv.put(KEY_VISITOR_ID, i);
        cv.put(COLUMN_TITLE, model.getTitle());
        cv.put(COLUMN_MESSAGE, model.getMessage());
        cv.put(COLUMN_IMAGE, model.getImage());
        cv.put(COLUMN_DATE, model.getCreatedAt());

        db.insert(TABLE_BULLETINE, null, cv);
        db.close();
    }

    public void addNotificationTable(Result model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        //cv.put(KEY_VISITOR_ID, i);
        cv.put(COLUMN_TITLE, model.getTitle());
        cv.put(COLUMN_MESSAGE, model.getMessage());
        cv.put(COLUMN_DATE, model.getCreatedAt());

        db.insert(TABLE_NOTIFICATION, null, cv);
        db.close();
    }

    public List<Bulletin> getAllBulletineData() {
        ArrayList<Bulletin> bulletinArrayList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_BULLETINE +" ORDER BY " + KEY_NOTIFICATION_PRIM_KEY + " DESC",null);

        // looping through all rows and adding to list
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Bulletin rs = new Bulletin(
                            cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_DATE))
                    );
                    // Adding practice tests to list
                    bulletinArrayList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        // return contact list
        return bulletinArrayList;
    }

    public List<NotificationDB> getAllNotificationData1() {
        ArrayList<NotificationDB> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        //    Getting data from database table
        //Cursor cursor = db.rawQuery("select * from " + TABLE_NOTIFICATION , null);
        //Cursor cursor = db.rawQuery("select * from " + TABLE_NOTIFICATION + "order by " + COLUMN_ID + "desc", null);
        //Cursor cursor = db.query("select * from " + TABLE_NOTIFICATION, null,null,null,null, null,  + " DESC", null);
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION +" ORDER BY " + KEY_NOTIFICATION_PRIM_KEY + " DESC",null);
        //Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION + " ORDER BY " + KEY_NOTIFICATION_PRIM_KEY + " DESC LIMIT 1 ", null);
        //Cursor cursor = db.rawQuery("SELECT distinct id,title, messsage, date FROM TABLE_NOTIFICATION order by id desc",new String[]{});

        // looping through all rows and adding to list
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    NotificationDB rs = new NotificationDB(
                            cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_DATE))
                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        // return contact list
        return countVisitorList;
    }

    public List<Result> getAllNotificationData() {
        ArrayList<Result> countVisitorList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        //    Getting data from database table
        //Cursor cursor = db.rawQuery("select * from " + TABLE_NOTIFICATION , null);
        //Cursor cursor = db.rawQuery("select * from " + TABLE_NOTIFICATION + "order by " + COLUMN_ID + "desc", null);
        //Cursor cursor = db.query("select * from " + TABLE_NOTIFICATION, null,null,null,null, null,  + " DESC", null);
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION +" ORDER BY " + KEY_NOTIFICATION_PRIM_KEY + " DESC",null);
        //Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION + " ORDER BY " + KEY_NOTIFICATION_PRIM_KEY + " DESC LIMIT 1 ", null);
        //Cursor cursor = db.rawQuery("SELECT distinct id,title, messsage, date FROM TABLE_NOTIFICATION order by id desc",new String[]{});

        // looping through all rows and adding to list
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    /*NotificationDB rs = new NotificationDB(
                            cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_DATE))*/

                    Result rs = new Result(
                            cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_DATE))

                    );
                    // Adding practice tests to list
                    countVisitorList.add(rs);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        // return contact list
        return countVisitorList;
    }

    /*public void insertNotificationDB(NotificationDB notificationDB) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("DROP TABLE IF EXISTS notification_records");
        writableDatabase.execSQL(NotificationDB.CREATE_TABLE);
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationDB.COLUMN_ID, notificationDB.getId());
        contentValues.put(NotificationDB.COLUMN_TITLE, notificationDB.getTitle());
        contentValues.put(NotificationDB.COLUMN_MESSAGE, notificationDB.getMessage());
        writableDatabase.insert(NotificationDB.TABLE_NAME, null, contentValues);
        writableDatabase.close();
    }

    public ArrayList<NotificationDB> getAllNotifications() {
        ArrayList<NotificationDB> arrayList = new ArrayList<>();
        String str = "SELECT  * FROM notification_records";
        SQLiteDatabase writableDatabase = getWritableDatabase();
        try {
            Cursor rawQuery = writableDatabase.rawQuery(str, null);
            if (rawQuery.moveToFirst()) {
                do {
                    NotificationDB attributes = new NotificationDB();
                    attributes.setId(rawQuery.getString(rawQuery.getColumnIndex(NotificationDB.COLUMN_ID)));
                    attributes.setTitle(rawQuery.getString(rawQuery.getColumnIndex(NotificationDB.COLUMN_TITLE)));
                    attributes.setMessage(rawQuery.getString(rawQuery.getColumnIndex(NotificationDB.COLUMN_MESSAGE)));
                    arrayList.add(attributes);
                } while (rawQuery.moveToNext());
            }
            writableDatabase.close();
            return arrayList;
        } catch (Exception unused) {
            return arrayList;
        }
    }

    public void insertNotificationDB1(ArrayList<NotificationDB> arrayList) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        writableDatabase.execSQL("DROP TABLE IF EXISTS notification_records");
        writableDatabase.execSQL(NotificationDB.CREATE_TABLE);
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < arrayList.size(); i++) {
            contentValues.put(NotificationDB.COLUMN_ID, ((NotificationDB) arrayList.get(i)).getId());
            contentValues.put(NotificationDB.COLUMN_TITLE, ((NotificationDB) arrayList.get(i)).getTitle());
            contentValues.put(NotificationDB.COLUMN_MESSAGE, ((NotificationDB) arrayList.get(i)).getMessage());
            writableDatabase.insert(NotificationDB.TABLE_NAME, null, contentValues);
        }
        writableDatabase.close();
    }*/

}
