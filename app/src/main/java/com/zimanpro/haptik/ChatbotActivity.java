package com.zimanpro.haptik;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.zimanpro.R;
import com.zimanpro.helperClasses.SessionManager;

import ai.haptik.android.sdk.Callback;
import ai.haptik.android.sdk.HaptikException;
import ai.haptik.android.sdk.HaptikLib;
import ai.haptik.android.sdk.SignUpData;
import ai.haptik.android.sdk.data.model.User;
import ai.haptik.android.sdk.messaging.MessagingClient;
import ai.haptik.android.sdk.messaging.MessagingEventListener;

public class ChatbotActivity extends AppCompatActivity implements LifecycleOwner {

    SessionManager sessionManager;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
        setContentView(R.layout.activity_chatbot);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        sessionManager = new SessionManager(this);


        launchInbox();


        MessagingClient.getInstance().setMessagingEventListener(new MessagingEventListener() {
            @Override
            public void onUnreadMessageCountChanged(int unreadMessageCount) {
                int totalUnreadMessages = unreadMessageCount;
                System.out.println("ChatbotActivity Unread Messages = " + unreadMessageCount);
            }
        });

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void launchInbox() {
        // Check if Haptik is initialized or not. If not initialized then initialize it before doing anything
        // Haptik would  be initialized if you init once, went into inbox, and come back. Then user press the haptik button again
        // In this demo app, it's initialized in the application class itself, so it would be always initialized here but we have just
        // kept it here for those cases where client app is not initializing the library in application class
        if (!HaptikLib.isInitialized()) {
         //   HaptikLib.setRunEnvironment(HaptikLib.RUN_ENVIRONMENT_STAGING);
            HaptikLib.setRunEnvironment(HaptikLib.RUN_ENVIRONMENT_PRODUCTION);
            ai.haptik.android.sdk.InitData initData = new  ai.haptik.android.sdk.InitData.Builder(getApplication())
                    .baseUrl("https://ziman.haptikapi.com/")
                //      .baseUrl("https://api.haptikapi.com/")
                //     .baseUrl("https://staging.hellohaptik.com/")
                     .notificationSound(R.raw.beep)
                    .build();
            HaptikLib.init(initData);
        }
        // Check if haptik's initial data has been sync once or not.
        // It's important to sync the initial data once before doing anything because otherwise sdk won't have any data to show on inbox
        // Once initial data is fetched, there is no need to call this method again because sdk will then handle syncing data internally
        if (!hasHaptikInitialDataSyncDone()) {
            HaptikLib.performInitialDataSync(new Callback<Boolean>() {
                @Override
                public void success(Boolean result) {
                    // On Success - set the flag to true so that we don't call this method in next app launch
                    Utils.setHaptikInitialDataSyncDone(ChatbotActivity.this, true);
                    onHaptikDataSyncOnce();
                }

                @Override
                public void failure(HaptikException exception) {
//                    toggleUiState(false);
                    // Handle Failure - If this fails then you cannot start Haptik. May call performInitialDataSync again
                    Toast.makeText(ChatbotActivity.this, "Haptik Init Data Sync Failed.\nCan't move ahead!", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            onHaptikDataSyncOnce();
        }
    }

    private boolean hasHaptikInitialDataSyncDone() {
        SharedPreferences sharedPreferences = getSharedPreferences(Utils.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(Utils.PREFS_KEY_HAPTIK_DATA_SYNC_ONCE, false);
    }

    // Check if user is already logged in or not
    // if Logged in then open inbox
    // if not logged then go through signUp flow
    private void onHaptikDataSyncOnce() {
        if (HaptikLib.isUserLoggedIn()) {
//            toggleUiState(false);
            goToInbox();
        } else {
            performSignUp();
        }
    }

    private void performSignUp() {

//        SignUpData signUpData = new SignUpData.Builder(SignUpData.AUTH_TYPE_BASIC)
//                .userFullName(sessionManager.getUserName())
//                .userPhone(sessionManager.getMobileNo())
//                .authToken("454af59c6c29435cb5e5aa0cada12345").build();

        SignUpData signUpData = new SignUpData.Builder(SignUpData.AUTH_TYPE_BASIC)
            .userFullName("HaptikLib Client User")
            .userEmail("demo@demo.com")
            .userPhone("7738027697")
            //.userCity("Mumbai")    //commented on 05072021
            .authToken("454af59c6c29435cb5e5aa0cada12345")
            //.shouldUseSmartWallet(true)        //commented on 05072021
            .build();

        HaptikLib.signUp(signUpData, new Callback<User>() {
            @Override
            public void success(User result) {
//                toggleUiState(false);
                Toast.makeText(ChatbotActivity.this, "SignUp success", Toast.LENGTH_SHORT).show();
                // On SignUp success, open inbox activity
                goToInbox();
            }

            @Override
            public void failure(HaptikException exception) {
//                toggleUiState(false);
                finish();
                Toast.makeText(ChatbotActivity.this, "SignUp failure", Toast.LENGTH_SHORT).show();
                // Handle SignUp failure, Haptik/inbox must not be opened
                exception.printStackTrace();
            }
        });
    }


    private void goToInbox() {
        Intent intent = new Intent(ChatbotActivity.this, InboxActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
