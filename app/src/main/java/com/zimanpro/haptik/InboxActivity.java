package com.zimanpro.haptik;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.zimanpro.R;

import java.util.List;

import ai.haptik.android.sdk.Callback;
import ai.haptik.android.sdk.HaptikException;
import ai.haptik.android.sdk.HaptikLib;
import ai.haptik.android.sdk.Router;
import ai.haptik.android.sdk.extensions.address.AddressHelper;
import ai.haptik.android.sdk.extensions.data.models.Address;
import ai.haptik.android.sdk.extensions.inbox.InboxView;
public class InboxActivity extends AppCompatActivity {

    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    InboxView view_inbox;
    MenuItem addMoneyMenuItem;

    // This will be called whenever an Intent with an action named "wallet-update" is broadcasted!
    private BroadcastReceiver walletReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //updateAddMoneyMenuItem();      //commented on 05072021
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!HaptikLib.isInitialized()) {
            ai.haptik.android.sdk.InitData initData = new ai.haptik.android.sdk.InitData.Builder(getApplication())
                    .baseUrl("https://ziman.haptikapi.com/")
                    .notificationSound(R.raw.beep)
                    .build();
            HaptikLib.init(initData);
        }
        setContentView(R.layout.activity_inbox);
        view_inbox = findViewById(R.id.inbox);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        addMoneyMenuItem = menu.findItem(R.id.action_add_money);
        //updateAddMoneyMenuItem();         //commented on 05072021
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_add_money:
                //Router.addMoneyToWallet(this);           //commented on 05072021
                finish();
                break;
            case R.id.action_wallet_history:
                //Router.showWalletHistory(this);          //commented on 05072021
                finish();
                break;
            case R.id.action_logout:
                // HaptikLib.logout();
                Utils.setHaptikInitialDataSyncDone(this, false);
                finish();
                break;
            case R.id.action_transaction_history:
//                final TransactionRequestor requestor = new TransactionRequestor();
//                requestor.fetchHistoryAsync(0, 30, new Callback<List<Transaction>>() {
//                    @Override
//                    public void success(List<Transaction> result) {
//                        for (Transaction transaction : result) {
//                            System.out.println(transaction.getId() + "@" + transaction.getBusinessName());
//                        }
//                    }
//
//                    @Override
//                    public void failure(HaptikException exception) {
//
//                    }
//                });
                break;
            case R.id.action_add_address:
                //Router.addAddress(this);
                break;
            case R.id.action_delete_addresses:
                deleteAllSavedAddresses();
                break;
            case R.id.action_print_addresses:
                AddressHelper.getAddressesAsync(new Callback<List<Address>>() {
                    @Override
                    public void success(List<Address> result) {
                        System.out.println(result);
                    }

                    @Override
                    public void failure(HaptikException exception) {

                    }
                });
                break;
        }
        return true;
    }

    private void deleteAllSavedAddresses() {
        AddressHelper.getAddressesAsync(new Callback<List<Address>>() {
            @Override
            public void success(List<Address> result) {
                for (Address address : result) {
                    AddressHelper.deleteAddress(InboxActivity.this, address, null);
                }
                if (!result.isEmpty()) {
                    Toast.makeText(InboxActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(InboxActivity.this, "No addresses found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(HaptikException exception) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        view_inbox.onResume();
        // Update latest wallet balance on menu UI
        //updateAddMoneyMenuItem();                //commented on 05072021
        // Register broadcast receiver for wallet balance update
        //commented on 05072021
        /*final IntentFilter forWallet = new IntentFilter(HaptikLib.INTENT_FILTER_ACTION_WALLET_BALANCE);
        forWallet.addAction(HaptikLib.INTENT_FILTER_ACTION_WALLET_DOWN);
        LocalBroadcastManager.getInstance(this).registerReceiver(walletReceiver, forWallet);*/
    }

    // Update title of the menu item which current wallet balance in brackets
    //commented on 05072021
    /*private void updateAddMoneyMenuItem() {
        if (addMoneyMenuItem != null) {
            if (HaptikLib.isWalletCreated()) {
                addMoneyMenuItem.setVisible(true);
                if (HaptikLib.isWalletInformationAvailable()) {
                    addMoneyMenuItem.setTitle("\u20B9" + HaptikLib.getWalletBalance());
                } else if (HaptikLib.isSmartWalletDown()) {
                    addMoneyMenuItem.setTitle("\u20B9 --");
                }
            } else {
                addMoneyMenuItem.setVisible(false);
            }
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        // Pause InboxView
        view_inbox.onPause();
        // Unregister the wallet balance broadcast
        LocalBroadcastManager.getInstance(this).unregisterReceiver(walletReceiver);
    }
}