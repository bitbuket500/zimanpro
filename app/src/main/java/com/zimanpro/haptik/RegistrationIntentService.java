package com.zimanpro.haptik;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.zimanpro.R;

import java.io.IOException;

import ai.haptik.android.sdk.HaptikLib;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        try {
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            System.out.println("RegistrationService TOken = " + token);
            // notify the token to sdk
            HaptikLib.setDeviceToken(getApplicationContext(), token);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
