package com.zimanpro.interfaces;

public class AppDataUrls {

    public static final String BASE_URL = "https://zimanb2c.citysmile.in/backend/api/";
    //public static final String BASE_URL = "http://citysmile.in/citysmile.in/ziman_backend/backend/api/";
    //public static final String BASE_URL = "http://citysmile.in/citysmile.in/say_mahindrafinance/backend/api/";

    public AppDataUrls() {
    }

    public static String postLogin() {
        return BASE_URL + "login";
    }

    public static String signup() {
        return BASE_URL + "signup";
    }

    public static String postSentOTP() {
        return BASE_URL + "sendOTP";
    }

    public static String postVerifyOTP() {
        return BASE_URL + "verifyOTP";
    }

    public static String getPlans() {
        return BASE_URL + "getPlans";
    }

    public static String updatePlans() {
        return BASE_URL + "updatePlans";
    }

    public static String getUserDetails() {
        return BASE_URL + "getUserDetails";
    }

    public static String updateUserDetails() {
        return BASE_URL + "updateUserDetails";
    }

    public static String postMedia() {
        return BASE_URL + "saveMultimedia";
    }

    public static String getEmergencyContacts() {
        return BASE_URL + "getEmergencyContacts";
    }

    public static String postSetTracking() {
        return BASE_URL + "setTracking";
    }

    public static String postUpdateTracking() {
        return BASE_URL + "updateTracking";
    }

    public static String postGetTracking() {
        return BASE_URL + "getTracking";
    }

    public static String postCreatePanicRequest() {
        return BASE_URL + "createPanicRequest";
    }

    public static String postAboutUsNPolicty() {
        return BASE_URL + "pages";
    }

    public static String postRaiseAQuery() {
        return BASE_URL + "raiseQuery";
    }

    public static String postPreTriggerNotification() {
        return BASE_URL + "preTriggerNotification";
    }

    public static String getNotifications() {
        return BASE_URL + "getNotifications";
    }

    public static String getQueryType() {
        return BASE_URL + "getQueryType";
    }

    public static String updateDeviceToken() {
        return BASE_URL + "updateDeviceToken";
    }

}
