package com.zimanpro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MultimediaResponse implements Serializable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public String result;
}
