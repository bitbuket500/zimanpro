package com.zimanpro.pojo.get_notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationListResponse implements Serializable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("result")
    @Expose
    public ArrayList<Result> result = null;

    @SerializedName("message")
    @Expose
    public String message;



}
