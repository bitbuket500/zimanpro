package com.zimanpro.pojo.get_plans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("price")
    @Expose
    public String price;

}
